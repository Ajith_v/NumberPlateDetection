import { Component } from '@angular/core';
import { HttpModule,Headers,Http } from '@angular/http';
import { map } from 'rxjs/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  numberplate: any;
  number: any;
  showToggle: any=false;
  splitedData: any=[];
  imageUrl:any='';
  data: any;
  constructor(public http:Http) {


   
  



  }
 submit() {
   this.showToggle=true;
  const toDataURL = url => fetch(url)
  .then(response => response.blob())
  .then(blob => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.onerror = reject
    reader.readAsDataURL(blob)
   }))
if(this.imageUrl) {
  toDataURL(this.imageUrl)
  .then(dataUrl => {
     this.data=dataUrl;
     this.splitedData=this.data.split(',');
     console.log(this.splitedData);
     this.getNumber();
   })
}


 }
 getNumber() {
   let imageData=new FormData();
   if(this.splitedData[1]) {
    imageData.append('image',this.splitedData[1]);
    this.http.post('http://api.henprs.ml/check.php',imageData).subscribe(res=>{
       this.number=res.json();
     //  console.log(this.number.data.results[0].plate);
       this.numberplate=this.number.data.results[0].plate;
       console.log(  this.numberplate)

    },
   err=>{
     console.log(err)
   })

   }
 
   
  
 }


}
